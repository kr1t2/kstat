package org.example.krit.kstat;

import org.bukkit.plugin.java.JavaPlugin;
import org.example.krit.kstat.commands.StatCommand;
import org.example.krit.kstat.commands.StatTabCompleter;

public final class KStat extends JavaPlugin {

    private static KStat plugin;

    @Override
    public void onEnable() {
        // Plugin startup logic
        plugin = this;

        getCommand("stat").setExecutor(new StatCommand());
        getCommand("stat").setTabCompleter(new StatTabCompleter());

        getConfig().options().copyDefaults();
        saveDefaultConfig();
    }

    public static KStat getInstance() {
        return plugin;
    }
}
