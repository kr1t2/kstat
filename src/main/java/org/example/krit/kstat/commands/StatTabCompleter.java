package org.example.krit.kstat.commands;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.util.StringUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StatTabCompleter implements org.bukkit.command.TabCompleter {

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        if (args.length == 1) {
            String[] statNames = {
                    "help",
                    "ANIMALS_BRED",
                    "ARMOR_CLEANED",
                    "AVIATE_ONE_CM",
                    "BANNER_CLEANED",
                    "BEACON_INTERACTION",
                    "BELL_RING",
                    "BOAT_ONE_CM",
                    "BREAK_ITEM",
                    "BREWINGSTAND_INTERACTION",
                    "CAKE_SLICES_EATEN",
                    "CAULDRON_FILLED",
                    "CAULDRON_USED",
                    "CHEST_OPENED",
                    "CLEAN_SHULKER_BOX",
                    "CLIMB_ONE_CM",
                    "CRAFT_ITEM",
                    "CRAFTING_TABLE_INTERACTION",
                    "CROUCH_ONE_CM",
                    "DAMAGE_ABSORBED",
                    "DAMAGE_BLOCKED_BY_SHIELD",
                    "DAMAGE_DEALT",
                    "DAMAGE_DEALT_ABSORBED",
                    "DAMAGE_DEALT_RESISTED",
                    "DAMAGE_RESISTED",
                    "DAMAGE_TAKEN",
                    "DEATHS",
                    "DISPENSER_INSPECTED",
                    "DROP",
                    "DROP_COUNT",
                    "DROPPER_INSPECTED",
                    "ENDERCHEST_OPENED",
                    "ENTITY_KILLED_BY",
                    "FALL_ONE_CM",
                    "FISH_CAUGHT",
                    "FLOWER_POTTED",
                    "FLY_ONE_CM",
                    "FURNACE_INTERACTION",
                    "HOPPER_INSPECTED",
                    "HORSE_ONE_CM",
                    "INTERACT_WITH_ANVIL",
                    "INTERACT_WITH_BLAST_FURNACE",
                    "INTERACT_WITH_CAMPFIRE",
                    "INTERACT_WITH_CARTOGRAPHY_TABLE",
                    "INTERACT_WITH_GRINDSTONE",
                    "INTERACT_WITH_LECTERN",
                    "INTERACT_WITH_LOOM",
                    "INTERACT_WITH_SMITHING_TABLE",
                    "INTERACT_WITH_SMOKER",
                    "INTERACT_WITH_STONECUTTER",
                    "ITEM_ENCHANTED",
                    "JUMP",
                    "KILL_ENTITY",
                    "LEAVE_GAME",
                    "MINE_BLOCK",
                    "MINECART_ONE_CM",
                    "MOB_KILLS",
                    "NOTEBLOCK_PLAYED",
                    "NOTEBLOCK_TUNED",
                    "OPEN_BARREL",
                    "PICKUP",
                    "PIG_ONE_CM",
                    "PLAY_ONE_MINUTE",
                    "RAID_TRIGGER",
                    "RAID_WIN",
                    "RECORD_PLAYED",
                    "SHULKER_BOX_OPENED",
                    "SLEEP_IN_BED",
                    "SNEAK_TIME",
                    "SPRINT_ONE_CM",
                    "STRIDER_ONE_CM",
                    "SWIM_ONE_CM",
                    "TALKED_TO_VILLAGER",
                    "TARGET_HIT",
                    "TIME_SINCE_DEATH",
                    "TIME_SINCE_REST",
                    "TOTAL_WORLD_TIME",
                    "TRADED_WITH_VILLAGER",
                    "TRAPPED_CHEST_TRIGGERED",
                    "USE_ITEM",
                    "WALK_ON_WATER_ONE_CM",
                    "WALK_ONE_CM",
                    "WALK_UNDER_WATER_ONE_CM",
            };
            List<String> completions = new ArrayList<>();
            StringUtil.copyPartialMatches(args[0], List.of(statNames), completions);
            return completions;
        } else if (args.length == 2 && !args[0].equalsIgnoreCase("KILL_ENTITY") && !args[0].equalsIgnoreCase("MINE_BLOCK") && !args[0].equalsIgnoreCase("BREAK_ITEM") && !args[0].equalsIgnoreCase("CRAFT_ITEM") && !args[0].equalsIgnoreCase("ENTITY_KILLED_BY") && !args[0].equalsIgnoreCase("MOB_KILLS") && !args[0].equalsIgnoreCase("USE_ITEM")) {
            String[] completeStrings = {
                    "top",
                    "me",
                    "player",
                    "server",
            };
            List<String> completions = new ArrayList<>();
            StringUtil.copyPartialMatches(args[1], List.of(completeStrings), completions);
            return completions;
        } else if (args.length == 3 && args[1].equalsIgnoreCase("player") && (!args[0].equalsIgnoreCase("KILL_ENTITY") && !args[0].equalsIgnoreCase("MINE_BLOCK") && !args[0].equalsIgnoreCase("BREAK_ITEM") && !args[0].equalsIgnoreCase("CRAFT_ITEM") && !args[0].equalsIgnoreCase("ENTITY_KILLED_BY") && !args[0].equalsIgnoreCase("MOB_KILLS") && !args[0].equalsIgnoreCase("USE_ITEM"))) {
            ArrayList <String> playersNames = new ArrayList<>();
            for (OfflinePlayer people : Bukkit.getOfflinePlayers()) {
                playersNames.add(people.getName());
            }

            List<String> completions = new ArrayList<>();
            StringUtil.copyPartialMatches(args[2], playersNames, completions);
            return completions;
        } else if (args.length == 2 && (args[0].equalsIgnoreCase("KILL_ENTITY") || args[0].equalsIgnoreCase("ENTITY_KILLED_BY") || args[0].equalsIgnoreCase("MOB_KILLS"))) {
            List<String> enteties = Stream.of(EntityType.values())
                    .map(EntityType::name)
                    .collect(Collectors.toList());

            List<String> completions = new ArrayList<>();
            StringUtil.copyPartialMatches(args[1], enteties, completions);
            return completions;

        } else if (args.length == 2 && (args[0].equalsIgnoreCase("MINE_BLOCK") || args[0].equalsIgnoreCase("CRAFT_ITEM") || args[0].equalsIgnoreCase("BREAK_ITEM") || args[0].equalsIgnoreCase("ENTITY_KILLED_BY") || args[0].equalsIgnoreCase("MOB_KILLS") || args[0].equalsIgnoreCase("USE_ITEM")) ) {
            List<String> blocks = Stream.of(Material.values())
                    .map(Material::name)
                    .collect(Collectors.toList());

            List<String> completions = new ArrayList<>();
            StringUtil.copyPartialMatches(args[1], blocks, completions);
            return completions;

        } else if (args.length == 3 && (args[0].equalsIgnoreCase("KILL_ENTITY") || args[0].equalsIgnoreCase("MINE_BLOCK") || args[0].equalsIgnoreCase("BREAK_ITEM") || args[0].equalsIgnoreCase("CRAFT_ITEM") || args[0].equalsIgnoreCase("ENTITY_KILLED_BY") || args[0].equalsIgnoreCase("USE_ITEM") || args[0].equalsIgnoreCase("MOB_KILLS"))) {
            String[] completeStrings = {
                    "top",
                    "me",
                    "player",
                    "server",
            };

            List<String> completions = new ArrayList<>();
            StringUtil.copyPartialMatches(args[2], List.of(completeStrings), completions);
            return completions;
        } else if (args.length == 4 && args[2].equalsIgnoreCase("player") && (args[0].equalsIgnoreCase("KILL_ENTITY") || args[0].equalsIgnoreCase("MINE_BLOCK") || args[0].equalsIgnoreCase("BREAK_ITEM") || args[0].equalsIgnoreCase("CRAFT_ITEM") || args[0].equalsIgnoreCase("ENTITY_KILLED_BY") || args[0].equalsIgnoreCase("MOB_KILLS") || args[0].equalsIgnoreCase("USE_ITEM"))) {

            ArrayList <String> playersNames = new ArrayList<>();
            for (OfflinePlayer people : Bukkit.getOfflinePlayers()) {
                playersNames.add(people.getName());
            }

            List<String> completions = new ArrayList<>();
            StringUtil.copyPartialMatches(args[3], playersNames, completions);
            return completions;
        }

        return null;
    }
}
