package org.example.krit.kstat.commands;

import org.bukkit.*;
import org.bukkit.command.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.map.MinecraftFont;
import org.example.krit.kstat.KStat;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.bukkit.Bukkit.getConsoleSender;
import static org.bukkit.Bukkit.getServer;

public class StatCommand implements CommandExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        if (sender instanceof BlockCommandSender) {
            return true;
        }
        if (args.length == 0 || args[0].equalsIgnoreCase("help")) {
            sendMessage(sender, ChatColor.GOLD + "KStat - " + ChatColor.GREEN + "Посмотреть статистику:"+ ChatColor.YELLOW + " /stat <НАЗВАНИЕ СТАТИСТИКИ> <ПАРАМЕТРЫ СТАТИСТИКИ (если есть)> <ОДНО ИЗ НИХ: top, player, server, me> <ИМЯ ИГРОКА (если надо)>");
            sendMessage(sender, ChatColor.GOLD + "KStat - " + ChatColor.GREEN + "Пример: " + ChatColor.YELLOW + "/stat TOTAL_WORLD_TIME top");
            sendMessage(sender, ChatColor.GOLD + "KStat - " + ChatColor.GREEN + "Пример: " + ChatColor.YELLOW + "/stat USE_ITEM STONE player Kr1tX");

            return true;
        }

        String statName = args[0].toUpperCase();
        String commandSettings;
        try {
            if (statName.equalsIgnoreCase("MINE_BLOCK") || statName.equalsIgnoreCase("KILL_ENTITY") || statName.equalsIgnoreCase("CRAFT_ITEM") || statName.equalsIgnoreCase("BREAK_ITEM") || statName.equalsIgnoreCase("ENTITY_KILLED_BY") || statName.equalsIgnoreCase("MOB_KILLS") || statName.equalsIgnoreCase("USE_ITEM")) {
                commandSettings = args[2].toLowerCase();
            } else {
                commandSettings = args[1].toLowerCase();
            }
        } catch (Exception e) {
            sendMessage(sender, ChatColor.GOLD + "KStat - " + ChatColor.RED + "Введите параметры статистики до конца: /stat help");
            return true;
        }


        switch (commandSettings) {
            case "top":
                HashMap<String, Integer> statistic = new HashMap<>();
                int topLength;
                
                try {
                    if (statName.equalsIgnoreCase("MINE_BLOCK") || statName.equalsIgnoreCase("CRAFT_ITEM") || statName.equalsIgnoreCase("KILL_ENTITY") || statName.equalsIgnoreCase("BREAK_ITEM") || statName.equalsIgnoreCase("ENTITY_KILLED_BY") || statName.equalsIgnoreCase("MOB_KILLS") || statName.equalsIgnoreCase("USE_ITEM")) {
                        topLength = args.length < 4 ? KStat.getInstance().getConfig().getInt("default-top-length") : Integer.parseInt(args[3]);
                    } else {
                        topLength = args.length < 3 ? KStat.getInstance().getConfig().getInt("default-top-length") : Integer.parseInt(args[2]);
                    }

                    if (topLength > KStat.getInstance().getConfig().getInt("max-top-length")) {
                        topLength = KStat.getInstance().getConfig().getInt("max-top-length");
                    }
                } catch (Exception e) {

                    sendMessage(sender, ChatColor.GOLD + "KStat - " + ChatColor.RED + "Введите число от 1 в третьем параметре или уберите его.");
                    return true;
                }

                for (OfflinePlayer p : getServer().getOfflinePlayers()) {

                    try {
                        if (statName.equalsIgnoreCase("MINE_BLOCK") || statName.equalsIgnoreCase("CRAFT_ITEM") || statName.equalsIgnoreCase("BREAK_ITEM") || statName.equalsIgnoreCase("USE_ITEM")) {
                            statistic.put(p.getName(), p.getStatistic(Statistic.valueOf(statName), Material.valueOf(args[1])));
                        } else if (statName.equalsIgnoreCase("KILL_ENTITY") || statName.equalsIgnoreCase("ENTITY_KILLED_BY") || statName.equalsIgnoreCase("MOB_KILLS")) {
                            statistic.put(p.getName(), p.getStatistic(Statistic.valueOf(statName), EntityType.valueOf(args[1])));
                        } else {
                            statistic.put(p.getName(), p.getStatistic(Statistic.valueOf(statName)));
                        }
                    } catch (Exception e) {
                        if (statName.equalsIgnoreCase("MINE_BLOCK") || statName.equalsIgnoreCase("KILL_ENTITY") || statName.equalsIgnoreCase("CRAFT_ITEM") || statName.equalsIgnoreCase("BREAK_ITEM") || statName.equalsIgnoreCase("ENTITY_KILLED_BY") || statName.equalsIgnoreCase("MOB_KILLS") || statName.equalsIgnoreCase("USE_ITEM")) {
                            sendMessage(sender, "Не существует данного материала или сущности");
                            return true;
                        }
                        sendMessage(sender, "Не существует данной статистики или произошла другая ошибка");
                        return true;
                    }

                }
                Map<String, Integer> sortedStat = statistic.entrySet()
                        .stream()
                        .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                        .collect(Collectors.toMap(
                                Map.Entry::getKey,
                                Map.Entry::getValue,
                                (oldValue, newValue) -> oldValue, LinkedHashMap::new));

                int id = 1;
                if (statName.equalsIgnoreCase("MINE_BLOCK") || statName.equalsIgnoreCase("KILL_ENTITY") || statName.equalsIgnoreCase("CRAFT_ITEM") || statName.equalsIgnoreCase("BREAK_ITEM") || statName.equalsIgnoreCase("ENTITY_KILLED_BY") || statName.equalsIgnoreCase("MOB_KILLS") || statName.equalsIgnoreCase("USE_ITEM")) {
                    sendMessage( sender, ChatColor.GOLD + "KStat" + ChatColor.WHITE + " - " + ChatColor.GREEN + statName + " - " + args[1] +  ChatColor.GRAY + " Top " + topLength);
                } else {
                    sendMessage( sender, ChatColor.GOLD + "KStat" + ChatColor.WHITE + " - " + ChatColor.GREEN + statName + ChatColor.GRAY + " Top " + topLength);
                }

                for (Map.Entry<String, Integer> entry : sortedStat.entrySet()) {
                    if (id > topLength) {
                        break;
                    }
                    String playerName = entry.getKey();
                    int statNum = entry.getValue();
                    if (statNum == 0 && id == 1) {
                        sendMessage(sender, ChatColor.RED + "Пока что никто не делал этого...");
                        break;
                    }

                        if (statName.equals("TOTAL_WORLD_TIME")) {
                            String totalWorldTime = millisecToTime(statNum * 50);
                            sendMessage(sender, ChatColor.GOLD
                                    + "" + id + ". " +
                                    ChatColor.GREEN + playerName +
                                    ChatColor.DARK_GRAY + ".".repeat((int) Math.round((230.0 - MinecraftFont.Font.getWidth(playerName + id + totalWorldTime)) / 2)) +
                                    ChatColor.BLUE + totalWorldTime);
                        } else {
                            sendMessage(sender, ChatColor.GOLD
                                    + "" + id + ". " +
                                    ChatColor.GREEN + playerName +
                                    ChatColor.DARK_GRAY + ".".repeat((int) Math.round((230.0 - MinecraftFont.Font.getWidth(playerName + id + statNum)) / 2)) +
                                    ChatColor.BLUE + statNum);
                        }

                    id++;
                }

                sendMessage(sender, ChatColor.DARK_GRAY + "-".repeat(40));

                return true;
            case "player":
                OfflinePlayer playerStatTarget;
                try {
                    if (statName.equalsIgnoreCase("MINE_BLOCK") || statName.equalsIgnoreCase("KILL_ENTITY") || statName.equalsIgnoreCase("CRAFT_ITEM") || statName.equalsIgnoreCase("BREAK_ITEM") || statName.equalsIgnoreCase("ENTITY_KILLED_BY") || statName.equalsIgnoreCase("MOB_KILLS") || statName.equalsIgnoreCase("USE_ITEM")) {
                        playerStatTarget = Bukkit.getOfflinePlayer(args[3]);
                    } else {
                        playerStatTarget = Bukkit.getOfflinePlayer(args[2]);
                    }

                } catch (Exception e) {
                    sendMessage(sender, ChatColor.GOLD + "KStat - " + ChatColor.RED + "Введите существующего игрока");
                    return true;
                }

                int playerStatNum;

                if (statName.equalsIgnoreCase("MINE_BLOCK") || statName.equalsIgnoreCase("CRAFT_ITEM") || statName.equalsIgnoreCase("BREAK_ITEM") || statName.equalsIgnoreCase("USE_ITEM")) {
                    try {
                        playerStatNum = playerStatTarget.getStatistic(Statistic.valueOf(statName), Material.valueOf(args[1]));
                    } catch (Exception e) {
                        sendMessage(sender, ChatColor.GOLD + "KStat - " + ChatColor.RED + "Не существует данного материала: " + args[1]);
                        return true;
                    }
                } else if (statName.equalsIgnoreCase("KILL_ENTITY") || statName.equalsIgnoreCase("ENTITY_KILLED_BY") || statName.equalsIgnoreCase("MOB_KILLS")) {
                    try {
                        playerStatNum = playerStatTarget.getStatistic(Statistic.valueOf(statName), EntityType.valueOf(args[1]));
                    } catch (Exception e) {
                        sendMessage(sender, ChatColor.GOLD + "KStat - " + ChatColor.RED + "Не существует данного существа: " + args[1]);
                        return true;
                    }
                } else {
                    try {
                        playerStatNum = playerStatTarget.getStatistic(Statistic.valueOf(statName));
                    } catch (Exception e) {
                        sendMessage(sender, ChatColor.GOLD + "KStat - " + ChatColor.RED + "Не существует данной статистики: " + statName);
                        return true;
                    }
                }



                    if (statName.equalsIgnoreCase("MINE_BLOCK") || statName.equalsIgnoreCase("KILL_ENTITY") || statName.equalsIgnoreCase("CRAFT_ITEM") || statName.equalsIgnoreCase("BREAK_ITEM") || statName.equalsIgnoreCase("ENTITY_KILLED_BY") || statName.equalsIgnoreCase("MOB_KILLS") || statName.equalsIgnoreCase("USE_ITEM")) {
                        sendMessage(sender, ChatColor.GOLD + "KStat" + ChatColor.WHITE + " - " + ChatColor.GREEN + statName + " - " + args[1] + ChatColor.GRAY + " Игрок " + ChatColor.GOLD + playerStatTarget.getName() );
                    } else {
                        sendMessage(sender, ChatColor.GOLD + "KStat" + ChatColor.WHITE + " - " + ChatColor.GREEN + statName + ChatColor.GRAY + " Игрок " + ChatColor.GOLD + playerStatTarget.getName() );
                    }

                    if (statName.equals("TOTAL_WORLD_TIME")) {
                        String totalWorldTime = millisecToTime(playerStatNum * 50);
                        sendMessage(sender,
                                ChatColor.GOLD + playerStatTarget.getName() +
                                        ChatColor.DARK_GRAY + ".".repeat((int) Math.round((230.0 - MinecraftFont.Font.getWidth(playerStatTarget.getName() + totalWorldTime)) / 2)) +
                                        ChatColor.BLUE + totalWorldTime);
                    } else {
                        sendMessage(sender,
                                ChatColor.GOLD + playerStatTarget.getName() +
                                        ChatColor.DARK_GRAY + ".".repeat((int) Math.round((230.0 - MinecraftFont.Font.getWidth(playerStatTarget.getName() + playerStatNum)) / 2)) +
                                        ChatColor.BLUE + playerStatNum);
                    }

                    sendMessage(sender, ChatColor.DARK_GRAY + "-".repeat(40));

                return true;
            case "me":
                if (sender instanceof Player p) {

                    int meStatNum;

                    if (statName.equalsIgnoreCase("MINE_BLOCK") || statName.equalsIgnoreCase("CRAFT_ITEM") || statName.equalsIgnoreCase("BREAK_ITEM") || statName.equalsIgnoreCase("USE_ITEM")) {
                        try {
                            meStatNum = p.getStatistic(Statistic.valueOf(statName), Material.valueOf(args[1]));
                        } catch (Exception e) {
                            sendMessage(sender, ChatColor.GOLD + "KStat - " + ChatColor.RED + "Не существует данного материала: " + args[1]);
                            return true;
                        }
                    } else if (statName.equalsIgnoreCase("KILL_ENTITY") || statName.equalsIgnoreCase("ENTITY_KILLED_BY") || statName.equalsIgnoreCase("MOB_KILLS")) {
                        try {
                            meStatNum = p.getStatistic(Statistic.valueOf(statName), EntityType.valueOf(args[1]));
                        } catch (Exception e) {
                            sendMessage(sender, ChatColor.GOLD + "KStat - " + ChatColor.RED + "Не существует данного существа: " + args[1]);
                            return true;
                        }
                    } else {
                        try {
                            meStatNum = p.getStatistic(Statistic.valueOf(statName));
                        } catch (Exception e) {
                            sendMessage(sender, ChatColor.GOLD + "KStat - " + ChatColor.RED + "Не существует данной статистики: " + statName);
                            return true;
                        }
                    }

                    if (statName.equalsIgnoreCase("MINE_BLOCK") || statName.equalsIgnoreCase("KILL_ENTITY") || statName.equalsIgnoreCase("CRAFT_ITEM") || statName.equalsIgnoreCase("BREAK_ITEM") || statName.equalsIgnoreCase("ENTITY_KILLED_BY") || statName.equalsIgnoreCase("MOB_KILLS") || statName.equalsIgnoreCase("USE_ITEM")) {
                        sendMessage(sender, ChatColor.GOLD + "KStat" + ChatColor.WHITE + " - " + ChatColor.GREEN + statName + " - " + args[1] + ChatColor.GRAY + " Игрок " + ChatColor.GOLD + p.getName());
                    } else {
                        sendMessage(sender, ChatColor.GOLD + "KStat" + ChatColor.WHITE + " - " + ChatColor.GREEN + statName + ChatColor.GRAY + " Игрок " + ChatColor.GOLD + p.getName());
                    }


                    if (statName.equals("TOTAL_WORLD_TIME")) {
                        String totalWorldTime = millisecToTime(meStatNum * 50);
                        sendMessage(sender,
                                ChatColor.GOLD + p.getName() +
                                ChatColor.DARK_GRAY + ".".repeat((int) Math.round((230.0 - MinecraftFont.Font.getWidth(p.getName() + totalWorldTime)) / 2)) +
                                ChatColor.BLUE + totalWorldTime);
                    } else {
                        sendMessage(sender,
                                ChatColor.GOLD + p.getName() +
                                ChatColor.DARK_GRAY + ".".repeat((int) Math.round((230.0 - MinecraftFont.Font.getWidth(p.getName() + meStatNum)) / 2)) +
                                ChatColor.BLUE + meStatNum);
                    }
                    p.sendMessage(ChatColor.DARK_GRAY + "-".repeat(40));
                } else if (sender instanceof ConsoleCommandSender) {
                    sendMessage(sender, ChatColor.GOLD + "KStat - " + ChatColor.RED + "К сожалению у консоли нет своей статистики");
                }
                return true;
            case "server":

                //Взять статистику со всего сервера и записать в переменнную
                int totalStatNum = 0;

                for (OfflinePlayer people : Bukkit.getOfflinePlayers()) {
                    if (statName.equals("MINE_BLOCK") || statName.equalsIgnoreCase("CRAFT_ITEM") || statName.equalsIgnoreCase("BREAK_ITEM") || statName.equalsIgnoreCase("USE_ITEM")) {
                        try {
                            totalStatNum += people.getStatistic(Statistic.valueOf(statName), Material.valueOf(args[1]));
                        } catch (Exception e) {
                            sendMessage(sender, ChatColor.GOLD + "KStat - " + ChatColor.RED + "Не существует данного материала: " + args[1]);
                            return true;
                        }
                    } else if (statName.equals("KILL_ENTITY") || statName.equalsIgnoreCase("ENTITY_KILLED_BY") || statName.equalsIgnoreCase("MOB_KILLS")) {
                        try {
                            totalStatNum += people.getStatistic(Statistic.valueOf(statName), EntityType.valueOf(args[1]));
                        } catch (Exception e) {
                            sendMessage(sender, ChatColor.GOLD + "KStat - " + ChatColor.RED + "Не существует данного существа: " + args[1]);
                            return true;
                        }
                    } else {
                        try {
                            totalStatNum += people.getStatistic(Statistic.valueOf(statName));
                        } catch (Exception e) {
                            sendMessage(sender, ChatColor.GOLD + "KStat - " + ChatColor.RED + "Не существует данной статистики: " + statName);
                            return true;
                        }
                    }
                }

                if (totalStatNum == 0) {
                    sendMessage(sender, ChatColor.RED + "Пока что никто не делал этого...");
                    sendMessage(sender, ChatColor.DARK_GRAY + "-".repeat(40));
                    return true;
                }

                if (statName.equalsIgnoreCase("MINE_BLOCK") || statName.equalsIgnoreCase("CRAFT_ITEM") || statName.equals("KILL_ENTITY") || statName.equalsIgnoreCase("BREAK_ITEM") || statName.equalsIgnoreCase("ENTITY_KILLED_BY") || statName.equalsIgnoreCase("MOB_KILLS") || statName.equalsIgnoreCase("USE_ITEM")) {
                    sendMessage(sender, ChatColor.GOLD + "KStat" + ChatColor.WHITE + " - " + ChatColor.GREEN + statName + " - " + args[1] + ChatColor.GRAY + " Весь сервер");
                } else {
                    sendMessage(sender, ChatColor.GOLD + "KStat" + ChatColor.WHITE + " - " + ChatColor.GREEN + statName + ChatColor.GRAY + " Весь сервер");
                }

                if (statName.equals("TOTAL_WORLD_TIME")) {
                    String totalWorldTime = millisecToTime(totalStatNum * 50);
                    sendMessage(sender,
                            ChatColor.GOLD + "Весь сервер" +
                                    ChatColor.DARK_GRAY + ".".repeat((int) Math.round((230.0 - MinecraftFont.Font.getWidth("Vesb server" + totalWorldTime)) / 2)) +
                                    ChatColor.BLUE + totalWorldTime);
                } else {
                    sendMessage(sender,
                            ChatColor.GOLD + "Весь сервер" +
                                    ChatColor.DARK_GRAY + ".".repeat((int) Math.round((230.0 - MinecraftFont.Font.getWidth("Vesb server" + totalStatNum)) / 2)) +
                                    ChatColor.BLUE + totalStatNum);
                }

                sendMessage(sender, ChatColor.DARK_GRAY + "-".repeat(40));

                return true;
        }


        return true;
    }

    //-------------UTILS-------------

    public static String millisecToTime(long milliseconds) {
        final long day = TimeUnit.MILLISECONDS.toDays(milliseconds);

        final long hours = TimeUnit.MILLISECONDS.toHours(milliseconds)
                - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(milliseconds));

        final long minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds)
                - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliseconds));

        final long seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds)
                - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds));

        if (seconds == 0 && minutes == 0 && hours == 0 && day == 0) {
            return "Right now";
        } else if (minutes == 0 && hours == 0 && day == 0) {
            return String.format("%d s.", seconds);
        } else if (hours == 0 && day == 0) {
            return String.format("%d m. %d s.", minutes, seconds);
        } else if (day == 0) {
            return String.format("%d h. %d m. %d s.", hours, minutes, seconds);
        } else {
            return String.format("%d d. %d h. %d m. %d s.", day, hours, minutes, seconds);
        }
    }
    private void sendMessage(CommandSender sender, String message) {
        if (sender instanceof Player p) {
            p.sendMessage(message);
        } else if (sender instanceof ConsoleCommandSender) {
            getConsoleSender().sendMessage(message);
        }
    }
}

